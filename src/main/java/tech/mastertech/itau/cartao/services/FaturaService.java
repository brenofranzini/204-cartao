package tech.mastertech.itau.cartao.services;

import java.util.GregorianCalendar;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cartao.dto.Cartao;
import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.dto.Fatura;
import tech.mastertech.itau.cartao.dto.Lancamento;
import tech.mastertech.itau.cartao.repositories.CartaoRepository;
import tech.mastertech.itau.cartao.repositories.LancamentoRepository;

@Service
public class FaturaService {

	private Fatura fatura;

	@Autowired
	private LancamentoRepository lancamentoRepository;

	@Autowired
	private CartaoRepository cartaoRepository;

	public Fatura gerarFatura(String cartao) {
		fatura = null;
		Optional<Cartao> cart = cartaoRepository.findById(cartao);

		GregorianCalendar calendar = new GregorianCalendar();
		int mes = calendar.get(GregorianCalendar.MONTH) + 1;

		if (cart.isPresent()) {
			fatura = new Fatura();
			Cliente cliente = cart.get().getCliente();
			
			Iterable<Lancamento> lancamentosEncontrados = lancamentoRepository.findByCartao(cart.get());
			Iterable<Lancamento> lancamentos = StreamSupport.stream(
					lancamentosEncontrados.spliterator(),false)
					.filter(a -> (a.getData().getDayOfMonth() <= a.getCartao().getDataFechamento()
					&& a.getData().getMonthValue() == mes)
					|| (a.getData().getDayOfMonth() > a.getCartao().getDataFechamento()
							&& a.getData().getMonthValue() == mes - 1)
					|| (a.getData().getDayOfMonth() > a.getCartao().getDataFechamento()
							&& a.getData().getMonthValue() == 12))
			.collect(Collectors.toList());

//			Iterable<Lancamento> lanc = StreamSupport.stream(lancamentos.spliterator(), false)
//					.filter(a -> (a.getData().getDayOfMonth() <= a.getCartao().getDataFechamento()
//							&& a.getData().getMonthValue() == mes)
//							|| (a.getData().getDayOfMonth() > a.getCartao().getDataFechamento()
//									&& a.getData().getMonthValue() == mes - 1)
//							|| (a.getData().getDayOfMonth() > a.getCartao().getDataFechamento()
//									&& a.getData().getMonthValue() == 12))
//					.collect(Collectors.toList());

//			int valor1;
//			int valor2;
//			int valor3;
//
//			System.out.println("Mes atual: " + mes);
//			System.out.println();
//
//			for (Lancamento l : lancamentos) {
//				valor1 = l.getData().getDayOfMonth();
//				valor2 = l.getCartao().getDataFechamento();
//				valor3 = l.getData().getMonthValue();
//
//				System.out.println("Data Lancamento: " + valor1);
//				System.out.println("Data Fechamento: " + valor2);
//				System.out.println("Mes Lancamento: " + valor3);
//				System.out.println("Condicao1: " + (valor1 <= valor2 && valor3 == mes));
//				System.out.println("Condicao2: " + (valor1 > valor2 && valor3 == mes-1));
//				System.out.println("Condicao3: " + (valor1 > valor2 && valor3 == 12));
//				System.out.println("\n\n\n\n");
//			}

			fatura.setCartao(cart.get());
			fatura.setCliente(cliente);
			fatura.setDiaFechamento(cart.get().getDataFechamento());
			fatura.setLancamentos(lancamentos);
		}
		return fatura;
	}
}
