package tech.mastertech.itau.cartao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cartao.dto.Bandeira;
import tech.mastertech.itau.cartao.repositories.BandeiraRepository;

@Service
public class BandeiraService {
	@Autowired
	private BandeiraRepository bandeiraRepository;

	public Bandeira cadastrarBandeira(Bandeira bandeira) {
		return bandeiraRepository.save(bandeira);
	}

	public Bandeira obterBandeira(int id) {
		Optional<Bandeira> bandeira = bandeiraRepository.findById(id);
		if (bandeira.isPresent()) {
			return bandeira.get();
		}
		return null;
	}

	public Iterable<Bandeira> listarBandeiras() {
		return bandeiraRepository.findAll();
	}

//	public void deletar(int idBandeira) {
//		Bandeira bandeira = obterBandeira(idBandeira);
//		if (bandeira != null) {
//			bandeiraRepository.delete(bandeira);
//		}
//	}

}

