package tech.mastertech.itau.cartao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.repositories.ClienteRepository;

@Service
public class UsuarioService {

	@Autowired
	private ClienteRepository clienteRepository;

	public Cliente cadastrarCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	public Cliente obterCliente(int id) {
		Optional<Cliente> cliente = clienteRepository.findById(id);
		if (cliente.isPresent()) {
			return cliente.get();
		}
		return null;
	}

	public Iterable<Cliente> listarClientes() {
		return clienteRepository.findAll();
	}

	public Cliente atualizarCliente(int id, Cliente cliente) {
		Optional<Cliente> cl = clienteRepository.findById(id);
		
		if(cl.isPresent()) {
			Cliente c = cl.get();
			return clienteRepository.save(c);
		}
		return null;
	}
}
