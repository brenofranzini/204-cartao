package tech.mastertech.itau.cartao.services;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cartao.dto.Cartao;
import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.repositories.CartaoRepository;
import tech.mastertech.itau.cartao.repositories.ClienteRepository;

@Service
public class CartaoService {

	@Autowired
	private CartaoRepository cartaoRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	public Cartao cadastrarCartao(Cartao cartao) {
		cartao.setCartaoAtivo(true);
		return cartaoRepository.save(cartao);
	}

	public Cartao obterCartao(String id) {
		Optional<Cartao> cartao = cartaoRepository.findById(id);
		if (cartao.isPresent()) {
			return cartao.get();
		}
		return null;
	}

	public Iterable<Cartao> listarCartoes() {
		return cartaoRepository.findAll();
	}

	public Cartao vincularCartao(String cartao, Cliente cliente) {
		Optional<Cliente> c = clienteRepository.findById(cliente.getId());
		if (!c.isPresent()) {
			return null;
		}

		Optional<Cartao> cart = cartaoRepository.findById(cartao);
		if (!cart.isPresent()) {
			return null;
		}

		Cartao car = cart.get();
		car.setCliente(c.get());
		return cartaoRepository.save(car);
	}

	public Cartao atualizarStatus(String cartao, Map<String, Boolean> status) {
		Optional<Cartao> cart = cartaoRepository.findById(cartao);

		if (cart.isPresent()) {
			Boolean st = status.get("cartaoAtivo");

			if (st != null) {
				Cartao c = cart.get();
				c.setCartaoAtivo(st);

				return cartaoRepository.save(c);
			}
		}

		return null;
	}
}
