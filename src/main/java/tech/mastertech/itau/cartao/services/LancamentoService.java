package tech.mastertech.itau.cartao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.cartao.dto.Cartao;
import tech.mastertech.itau.cartao.dto.Lancamento;
import tech.mastertech.itau.cartao.repositories.CartaoRepository;
import tech.mastertech.itau.cartao.repositories.LancamentoRepository;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository lancamentoRepository;

	@Autowired
	private CartaoRepository cartaoRepository;

	public Lancamento cadastrarLancamento(Lancamento lancamento) {
		String cartao = lancamento.getCartao().getNumeroCartao();
		Optional<Cartao> cart = cartaoRepository.findById(cartao);

		if (cart.isPresent()) {
			lancamento.setCartao(cart.get());
			return lancamentoRepository.save(lancamento);
		}

		return null;
	}

	public Lancamento obterLancamento(long id) {
		Optional<Lancamento> lancamento = lancamentoRepository.findById(id);
		if (lancamento.isPresent()) {
			return lancamento.get();
		}
		return null;
	}

	public Iterable<Lancamento> listarLancamentos() {
		return lancamentoRepository.findAll();
	}
	
	public void deletarLancamento(long id) {		
		lancamentoRepository.deleteById(id);
	}
}
