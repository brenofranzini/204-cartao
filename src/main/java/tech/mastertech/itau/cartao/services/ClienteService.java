package tech.mastertech.itau.cartao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.repositories.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public Cliente cadastrarCliente(Cliente cliente) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		cliente.setSenha(encoder.encode(cliente.getSenha()));
		
		return clienteRepository.save(cliente);
	}

	public Cliente obterCliente(int id) {
		Optional<Cliente> cliente = clienteRepository.findById(id);
		if (cliente.isPresent()) {
			return cliente.get();
		}
		return null;
	}

	public Iterable<Cliente> listarClientes() {
		return clienteRepository.findAll();
	}

	public Cliente atualizarCliente(int id, Cliente cliente, boolean alterouSenha) {
		Optional<Cliente> cl = clienteRepository.findById(id);

		if (cl.isPresent()) {
			Cliente c = cl.get();

			if (alterouSenha) {
				BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
				c.setSenha(encoder.encode(c.getSenha()));
			}
			
			return clienteRepository.save(c);
		}
		return null;
	}

	public Cliente fazerLogin(Cliente cliente) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		if (encoder.matches(cliente.getSenha(), encoder.encode(cliente.getSenha()))) {
			Cliente c = clienteRepository.findByUsuario(cliente.getUsuario());
			return c;
		}
		return null;
	}

	public Cliente fazerCadastro(Cliente cliente) {
		Optional<Cliente> cl = clienteRepository.findById(cliente.getId());
		if (cl.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "O usuário informado no cadastro já existe");
		}
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String senha = cliente.getSenha();
		if (cliente.getUsuario().length() > 5 && senha.length() > 5) {
			cliente.setSenha(encoder.encode(senha));
			return clienteRepository.save(cliente);
		}
		return null;
	}
}
