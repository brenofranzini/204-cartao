package tech.mastertech.itau.cartao.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.dto.Bandeira;
import tech.mastertech.itau.cartao.services.BandeiraService;

@RequestMapping("/credito")
@RestController
public class BandeiraController {

	private Bandeira bandeira;

	@Autowired
	private BandeiraService BandeiraService;

	@PostMapping("/bandeira")
	@ResponseStatus(HttpStatus.CREATED)
	public Bandeira cadastrarBandeira(@RequestBody Bandeira Bandeira) {
		this.bandeira = BandeiraService.cadastrarBandeira(Bandeira);

		return this.bandeira;
	}

	@GetMapping("/bandeira/{id}")
	public Bandeira obterBandeira(@PathVariable int id) {
		bandeira = BandeiraService.obterBandeira(id);
		if (bandeira == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return bandeira;
	}
	
	@GetMapping("/bandeiras")
	public Iterable<Bandeira> listarBandeiras() {
		return BandeiraService.listarBandeiras();
	}
	
//	@DeleteMapping("/bandeira/{idBandeira}")
//	@ResponseStatus(HttpStatus.ACCEPTED)
//	public void deletar(@PathVariable int idBandeira) {
//		BandeiraService.deletar(idBandeira);
//	}
}
