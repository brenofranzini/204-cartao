package tech.mastertech.itau.cartao.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.services.ClienteService;

@RequestMapping("/credito")
@RestController
public class ClienteController {

	private Cliente cliente;
	private boolean alterouSenha;

	@Autowired
	private ClienteService clienteService;

	@PostMapping("/cliente")
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente cadastrarCliente(@RequestBody Cliente cliente) {
		this.cliente = clienteService.cadastrarCliente(cliente);

		return this.cliente;
	}

	@GetMapping("/cliente/{id}")
	public Cliente obterCliente(@PathVariable int id) {
		cliente = clienteService.obterCliente(id);
		if (cliente == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return cliente;
	}
	
	@GetMapping("/clientes")
	public Iterable<Cliente> listarClientes() {
		return clienteService.listarClientes();
	}
	
	@PatchMapping("/cliente/{id}")
	public Cliente atualizarCliente(@PathVariable int id,
			@RequestBody Map<String, Object> atributo) {
		cliente = montaCliente(atributo);
		return clienteService.atualizarCliente(id, cliente, alterouSenha);
	}
	
	private Cliente montaCliente(Map<String, Object> atributos) {
		alterouSenha = false;
		Cliente cliente = new Cliente();
		
		if(atributos.containsKey("nome")) {
			cliente.setNome(atributos.get("nome").toString());
		}
		
		if(atributos.containsKey("cpfCNPJ")) {
			cliente.setCpfCNPJ(atributos.get("cpfCNPJ").toString());
		}
		
		if(atributos.containsKey("tipoPessoa")) {
			cliente.setTipoPessoa(atributos.get("tipoPessoa").toString().charAt(0));
		}
		
		if(atributos.containsKey("senha")) {
			cliente.setSenha(atributos.get("senha").toString());
			alterouSenha = true;
		}
		
		return cliente;
	}
}
