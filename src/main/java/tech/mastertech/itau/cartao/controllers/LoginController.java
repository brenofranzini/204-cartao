package tech.mastertech.itau.cartao.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.security.JwtTokenProvider;
import tech.mastertech.itau.cartao.services.ClienteService;

@RestController
//@RequestMapping("/auth")
public class LoginController {

	@Autowired
	private ClienteService clienteService;

	@PostMapping("/login")
	public Map<String, Object> login(@RequestBody Cliente cliente) {
		Cliente clienteSalvo = clienteService.fazerLogin(cliente);

		if (clienteSalvo == null) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}

		Map<String, Object> retorno = new HashMap<>();
		retorno.put("usuario", clienteSalvo);

		JwtTokenProvider provider = new JwtTokenProvider();
		String token = provider.criarToken(cliente.getUsuario());
		retorno.put("token", token);

		return retorno;
	}
	
	@PostMapping("/cadastro")
	@ResponseStatus(HttpStatus.CREATED)
	public Map<String, Object> cadastrarUsuario(@RequestBody Cliente cliente) {
		Cliente clienteSalvo = clienteService.fazerCadastro(cliente);

		if (clienteSalvo == null) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}

		Map<String, Object> retorno = new HashMap<>();
		retorno.put("usuario", clienteSalvo);

		JwtTokenProvider provider = new JwtTokenProvider();
		String token = provider.criarToken(cliente.getUsuario());
		retorno.put("token", token);

		return retorno;
	}
}