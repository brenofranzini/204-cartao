package tech.mastertech.itau.cartao.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.dto.Lancamento;
import tech.mastertech.itau.cartao.services.LancamentoService;

@RequestMapping("/credito")
@RestController
public class LancamentoController {

	private Lancamento lancamento;

	@Autowired
	private LancamentoService lancamentoService;

	@PostMapping("/lancamento")
	@ResponseStatus(HttpStatus.CREATED)
	public Lancamento cadastrarLancamento(@RequestBody Lancamento lancamento) {
		this.lancamento = lancamentoService.cadastrarLancamento(lancamento);

		return this.lancamento;
	}

	@GetMapping("/lancamento/{id}")
	public Lancamento obterLancamento(@PathVariable int id) {
		lancamento = lancamentoService.obterLancamento(id);
		if (lancamento == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return lancamento;
	}
	
	@GetMapping("/lancamentos")
	public Iterable<Lancamento> listarLancamentos() {
		return lancamentoService.listarLancamentos();
	}
	
	@DeleteMapping("/lancamento/{idLancamento}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void deletar(@PathVariable long idLancamento) {
		lancamentoService.deletarLancamento(idLancamento);
	}
}
