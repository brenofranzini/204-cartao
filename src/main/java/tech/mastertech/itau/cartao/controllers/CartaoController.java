package tech.mastertech.itau.cartao.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.dto.Cartao;
import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.services.CartaoService;

@RequestMapping("/credito")
@RestController
public class CartaoController {

	private Cartao cartao;

	@Autowired
	private CartaoService cartaoService;

	@PostMapping("/cartao")
	@ResponseStatus(HttpStatus.CREATED)
	public Cartao cadastrarCartao(@RequestBody Cartao cliente) {
		this.cartao = cartaoService.cadastrarCartao(cliente);

		return this.cartao;
	}

	@GetMapping("/cartao/{id}")
	public Cartao obterCartao(@PathVariable String id) {
		cartao = cartaoService.obterCartao(id);
		if (cartao == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return cartao;
	}

	@GetMapping("/cartao")
	public Iterable<Cartao> listarCartoes() {
		return cartaoService.listarCartoes();
	}

	@PatchMapping("/cartao/{numero}/vinculo")
	public Cartao vincularCartao(@PathVariable String numero, @RequestBody Cliente cliente) {
		cartao = cartaoService.vincularCartao(numero, cliente);
		if (cartao == null) {
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
		}
		return cartao;
	}

	@PatchMapping("/cartao/{numero}/status")
	public Cartao atualizarStatusCartao(@PathVariable String numero, @RequestBody Map<String, Boolean> status) {
		cartao = cartaoService.atualizarStatus(numero, status);
		if (cartao == null) {
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
		}
		return cartao;
	}
	
	@GetMapping("/cartao/{numero}/fatura")
	public Cartao gerarFatura(@PathVariable String numero, @RequestBody Map<String, Boolean> status) {
		cartao = cartaoService.atualizarStatus(numero, status);
		if (cartao == null) {
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
		}
		return cartao;
	}
}
