package tech.mastertech.itau.cartao.dto;

public class Fatura {

	private Cliente cliente;
	private Cartao cartao;
	private Iterable<Lancamento> lancamentos;
	private int diaFechamento;

	public int getDiaFechamento() {
		return diaFechamento;
	}

	public void setDiaFechamento(int diaFechamento) {
		this.diaFechamento = diaFechamento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Cartao getCartao() {
		return cartao;
	}

	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}

	public Iterable<Lancamento> getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(Iterable<Lancamento> lancamentos) {
		this.lancamentos = lancamentos;
	}
}
