package tech.mastertech.itau.cartao.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.Length;

@Entity
public class Cartao {

	@Id
	@Length(min = 16, max = 16, message = "Preencher exatamente 16 caracteres")
	private String numeroCartao;
	
	private boolean cartaoAtivo;
	
	public boolean getStatusCartao() {
		return cartaoAtivo;
	}

	public void setCartaoAtivo(boolean cartaoAtivo) {
		this.cartaoAtivo = cartaoAtivo;
	}

	@ManyToOne
	private Bandeira bandeira;

	@ManyToOne
	private Cliente cliente;
	
	private int dataFechamento;

	public int getDataFechamento() {
		return dataFechamento;
	}

	public void setDataFechamento(int dataFechamento) {
		this.dataFechamento = dataFechamento;
	}

	public boolean isCartaoAtivo() {
		return cartaoAtivo;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public Bandeira getBandeira() {
		return bandeira;
	}

	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}

}
