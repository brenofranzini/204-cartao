package tech.mastertech.itau.cartao.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartao.dto.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
	public Cliente findByUsuario(String Usuario);

}
