package tech.mastertech.itau.cartao.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartao.dto.Cartao;
import tech.mastertech.itau.cartao.dto.Lancamento;

public interface LancamentoRepository extends CrudRepository<Lancamento, Long> {
	
	public Iterable<Lancamento> findByCartao(Cartao cartao);

}
