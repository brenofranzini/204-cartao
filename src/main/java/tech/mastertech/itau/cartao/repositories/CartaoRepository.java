package tech.mastertech.itau.cartao.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartao.dto.Cartao;

public interface CartaoRepository extends CrudRepository<Cartao, String>{

}
