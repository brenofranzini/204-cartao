package tech.mastertech.itau.cartao.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartao.dto.Bandeira;

public interface BandeiraRepository extends CrudRepository<Bandeira, Integer>{

}
