package tech.mastertech.itau.cartao.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.services.ClienteService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ClienteController.class)
public class ClienteControllerTest {
	private Cliente cliente;

	@Autowired
	private MockMvc mockMvc;
	private int id = 1;

	@MockBean
	private ClienteService clienteService;

	private ObjectMapper mapper = new ObjectMapper();

	@Before
	public void preparar() {
		cliente = new Cliente();

		cliente.setId(id);
		cliente.setNome("Breno");
		cliente.setCpfCNPJ("12345678910");
		cliente.setTipoPessoa('F');
	}

	@Test
	public void deveBuscarUmClientePorId() throws Exception {

		when(clienteService.obterCliente(cliente.getId())).thenReturn(cliente);

		System.out.println();

		mockMvc.perform(get("/credito/cliente/" + id)).andExpect(status().isOk())
				.andExpect(content().string(mapper.writeValueAsString(cliente)))
				.andExpect(content().string(containsString(String.valueOf((cliente.getId())))))
				.andExpect(content().string(containsString(cliente.getNome())))
				.andExpect(content().string(containsString(String.valueOf(cliente.getTipoPessoa()))))
				.andExpect(content().string(containsString(cliente.getCpfCNPJ())));
	}
	
	@Test
	public void deveBuscarUmaListaDeClientes() throws Exception {
		List<Cliente> clientes = Lists.newArrayList(cliente);

		when(clienteService.listarClientes()).thenReturn(clientes);

		System.out.println();

		mockMvc.perform(get("/credito/clientes")).andExpect(status().isOk())
				.andExpect(content().string(mapper.writeValueAsString(clientes)));
	}

	@Test
	public void deveCriarUmCliente() throws Exception {

		when(clienteService.cadastrarCliente(any(Cliente.class))).thenReturn(cliente);

		String clienteJson = mapper.writeValueAsString(cliente);
		System.out.println();
		
//		String token = new JwtTokenProvider().criarToken(teste);

		mockMvc.perform(post("/credito/cliente/")
			.content(clienteJson)
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isCreated())
			.andExpect(content().string(clienteJson));
	}
	
	@Test
	public void deveAlterarDadosDeUmCliente() throws Exception {
		
		cliente.setCpfCNPJ("66666666666");
		cliente.setNome("Batman");
		cliente.setTipoPessoa('J');
		
		when(clienteService.atualizarCliente(cliente.getId(), cliente)).thenReturn(cliente);
	    
	    Map<String, String> payload = new HashMap<>();
	    payload.put("nome", "Batman");
	    payload.put("cpfCNPJ", "66666666666");
	    payload.put("tipoPessoa", "J");
	    
	    String clienteJson = mapper.writeValueAsString(cliente);
	    String payloadJson = mapper.writeValueAsString(payload);
	    
	    mockMvc.perform(
	    		patch("/credito/cliente/" + cliente.getId())
	        .content(payloadJson)
	        .contentType(MediaType.APPLICATION_JSON_UTF8)
	    )
	    .andExpect(status().isOk())
	    .andExpect(content().string(clienteJson));
	  }

}
