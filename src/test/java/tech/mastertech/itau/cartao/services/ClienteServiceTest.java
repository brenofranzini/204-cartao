package tech.mastertech.itau.cartao.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.repositories.ClienteRepository;

@ContextConfiguration(classes = ClienteService.class)
@RunWith(SpringRunner.class)
public class ClienteServiceTest {

	private Cliente cliente;

	@Autowired
	private ClienteService sujeito;

	@MockBean
	private ClienteRepository clienteRepository;

	@Before
	public void preparar() {
		cliente = new Cliente();
		cliente.setNome("Breno");
		cliente.setTipoPessoa('F');
		cliente.setCpfCNPJ("1231231");
		cliente.setId(1);
	}

	@Test
	public void cadastrarCliente() {

		when(clienteRepository.save(cliente)).thenReturn(cliente);

		Cliente clienteSalvo = sujeito.cadastrarCliente(cliente);
		assertEquals(clienteSalvo.getNome(), cliente.getNome());
	}

	@Test
	public void deveRetornarListaDeClientes() {
		List<Cliente> clientes = Lists.list(cliente);

		when(clienteRepository.findAll()).thenReturn(clientes);

		List<Cliente> clientesEncontrados = Lists.newArrayList(sujeito.listarClientes());

		assertEquals(1, clientesEncontrados.size());
		assertEquals(cliente, clientesEncontrados.get(0));

	}

	@Test
	public void deveRetornarUmClienteEspecifico() {

//		when(clienteRepository.findById(cliente.getId()).get()).thenReturn(cliente);
		when(clienteRepository.findById(cliente.getId())).thenReturn(Optional.of(cliente));

		Cliente clienteSalvo = sujeito.obterCliente(cliente.getId());
		assertEquals(clienteSalvo.getNome(), cliente.getNome());

	}

	@Test
	public void naoDeveRetornarUmClienteEspecifico() {

		when(clienteRepository.findById(2)).thenReturn(Optional.empty());

		Cliente clienteSalvo = sujeito.obterCliente(cliente.getId());
		assertNull(clienteSalvo);

	}

	@Test
	public void deveAtualizarInformacoesDoCliente() {

		cliente.setNome("Teste");

		when(clienteRepository.findById(cliente.getId())).thenReturn(Optional.of(cliente));
		when(clienteRepository.save(cliente)).thenReturn(cliente);

		Cliente clienteSalvo = sujeito.atualizarCliente(cliente.getId(), cliente);
		assertEquals(clienteSalvo.getNome(), cliente.getNome());

	}

	@Test
	public void deveTentarAtualizarInformacoesDeClienteQueNaoExiste() {

		cliente.setNome("Teste");

		when(clienteRepository.findById(cliente.getId())).thenReturn(Optional.empty());
		when(clienteRepository.save(cliente)).thenReturn(cliente);

		Cliente clienteSalvo = sujeito.atualizarCliente(cliente.getId(), cliente);
		assertNull(clienteSalvo);
	}

//	@Test
//	public void deveDeletarUmCliente() {
//		int idCliente = 1;
//		
//		sujeito.deletar(idCliente);
//		
//		verify(clienteRepository).deleteById(idCliente);
//	}
}
