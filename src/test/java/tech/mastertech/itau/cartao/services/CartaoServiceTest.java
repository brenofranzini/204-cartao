package tech.mastertech.itau.cartao.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.cartao.dto.Bandeira;
import tech.mastertech.itau.cartao.dto.Cartao;
import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.repositories.BandeiraRepository;
import tech.mastertech.itau.cartao.repositories.CartaoRepository;
import tech.mastertech.itau.cartao.repositories.ClienteRepository;

@ContextConfiguration(classes = CartaoService.class)
@RunWith(SpringRunner.class)
public class CartaoServiceTest {

	private Cartao cartao;
	private Bandeira bandeira;
	private Cliente cliente;

	@Autowired
	private CartaoService sujeito;

	@MockBean
	private CartaoRepository cartaoRepository;

	@MockBean
	private ClienteRepository clienteRepository;

	@MockBean
	private BandeiraRepository bandeiraRepository;

	@Before
	public void preparar() {
		bandeira = new Bandeira();

		bandeira.setId(1);
		bandeira.setNome("VISA");

		cliente = new Cliente();

		cliente.setId(1);
		cliente.setNome("Breno");
		cliente.setTipoPessoa('F');
		cliente.setCpfCNPJ("12345678910");

		cartao = new Cartao();

		cartao.setNumeroCartao("5412123456781234");
		cartao.setCartaoAtivo(true);
		cartao.setBandeira(bandeira);
//		cartao.setCliente(cliente);
		cartao.setDataFechamento(23);
	}

	@Test
	public void cadastrarCartao() {

		when(cartaoRepository.save(cartao)).thenReturn(cartao);

		Cartao cartaoSalvo = sujeito.cadastrarCartao(cartao);

		assertEquals(cartaoSalvo.getBandeira(), cartao.getBandeira());
		assertEquals(cartaoSalvo.getDataFechamento(), cartao.getDataFechamento());
		assertEquals(cartaoSalvo.getNumeroCartao(), cartao.getNumeroCartao());
		assertEquals(cartaoSalvo.getStatusCartao(), cartao.getStatusCartao());
	}

	@Test
	public void deveRetornarListaDeCartaos() {
		List<Cartao> cartaos = Lists.list(cartao);

		when(cartaoRepository.findAll()).thenReturn(cartaos);

		List<Cartao> cartaosEncontrados = Lists.newArrayList(sujeito.listarCartoes());

		assertEquals(1, cartaosEncontrados.size());
		assertEquals(cartao, cartaosEncontrados.get(0));

	}

	@Test
	public void deveRetornarUmCartaoEspecifico() {

		when(cartaoRepository.findById(cartao.getNumeroCartao())).thenReturn(Optional.of(cartao));

		Cartao cartaoSalvo = sujeito.obterCartao(cartao.getNumeroCartao());
		assertEquals(cartaoSalvo.getNumeroCartao(), cartao.getNumeroCartao());

	}

	@Test
	public void naoDeveRetornarUmCartaoEspecifico() {

		when(cartaoRepository.findById("1234567891234567")).thenReturn(Optional.empty());

		Cartao cartaoSalvo = sujeito.obterCartao(cartao.getNumeroCartao());
		assertNull(cartaoSalvo);

	}

	@Test
	public void vincularCartaoComClienteComSucesso() {

		cartao.setCliente(cliente);

		when(clienteRepository.findById(cliente.getId())).thenReturn(Optional.of(cliente));
		when(cartaoRepository.findById(cartao.getNumeroCartao())).thenReturn(Optional.of(cartao));
		when(cartaoRepository.save(cartao)).thenReturn(cartao);

		Cartao cartaoSalvo = sujeito.vincularCartao(cartao.getNumeroCartao(), cliente);

		assertEquals(cartaoSalvo.getBandeira(), cartao.getBandeira());
		assertEquals(cartaoSalvo.getCliente(), cartao.getCliente());
		assertEquals(cartaoSalvo.getDataFechamento(), cartao.getDataFechamento());
		assertEquals(cartaoSalvo.getNumeroCartao(), cartao.getNumeroCartao());
		assertEquals(cartaoSalvo.getStatusCartao(), cartao.getStatusCartao());
	}
	
	@Test
	public void vincularCartaoComClienteInvalido() {

		cartao.setCliente(cliente);

		when(clienteRepository.findById(cliente.getId())).thenReturn(Optional.empty());

		Cartao cartaoSalvo = sujeito.vincularCartao(cartao.getNumeroCartao(), cliente);

		assertNull(cartaoSalvo);
	}
	
	@Test
	public void vincularCartaoComClienteComCartaoInvalido() {

		cartao.setCliente(cliente);
		
		when(clienteRepository.findById(cliente.getId())).thenReturn(Optional.of(cliente));
		when(cartaoRepository.findById(cartao.getNumeroCartao())).thenReturn(Optional.empty());

		Cartao cartaoSalvo = sujeito.vincularCartao(cartao.getNumeroCartao(), cliente);

		assertNull(cartaoSalvo);
	}
	
	@Test
	public void atualizarStatusDoCartao() {

		cartao.setCartaoAtivo(false);
		
		when(cartaoRepository.findById(cartao.getNumeroCartao())).thenReturn(Optional.of(cartao));
		when(cartaoRepository.save(cartao)).thenReturn(cartao);

		Map<String, Boolean> valor = new HashMap<>();
		valor.put("cartaoAtivo", false);
		Cartao cartaoSalvo = sujeito.atualizarStatus(cartao.getNumeroCartao(), valor);

		assertEquals(cartaoSalvo.getStatusCartao(), valor.get("cartaoAtivo"));
	}
	
	@Test
	public void atualizarStatusDoCartaoParaCartaoNaoEncontrado() {

		cartao.setCartaoAtivo(false);
		
		when(cartaoRepository.findById(cartao.getNumeroCartao())).thenReturn(Optional.empty());
		when(cartaoRepository.save(cartao)).thenReturn(cartao);

		Map<String, Boolean> valor = new HashMap<>();
		valor.put("cartaoAtivo", false);
		Cartao cartaoSalvo = sujeito.atualizarStatus(cartao.getNumeroCartao(), valor);

		assertNull(cartaoSalvo);
	}
}
