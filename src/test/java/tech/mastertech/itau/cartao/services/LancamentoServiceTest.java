package tech.mastertech.itau.cartao.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.cartao.dto.Bandeira;
import tech.mastertech.itau.cartao.dto.Cartao;
import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.dto.Lancamento;
import tech.mastertech.itau.cartao.repositories.BandeiraRepository;
import tech.mastertech.itau.cartao.repositories.CartaoRepository;
import tech.mastertech.itau.cartao.repositories.ClienteRepository;
import tech.mastertech.itau.cartao.repositories.LancamentoRepository;

@ContextConfiguration(classes = LancamentoService.class)
@RunWith(SpringRunner.class)
public class LancamentoServiceTest {

	private Lancamento lancamento;
	private Bandeira bandeira;
	private Cliente cliente;
	private Cartao cartao;

	@Autowired
	private LancamentoService sujeito;

	@MockBean
	private LancamentoRepository lancamentoRepository;

	@MockBean
	private ClienteRepository clienteRepository;

	@MockBean
	private BandeiraRepository bandeiraRepository;

	@MockBean
	private CartaoRepository cartaoRepository;

	@Before
	public void preparar() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		String data1 = "2019-05-27";
//		String data2 = "2019-05-23";
//		String data3 = "2019-05-20";

		LocalDate localDate1 = LocalDate.parse(data1, formatter);
//		LocalDate localDate2 = LocalDate.parse(data2, formatter);
//		LocalDate localDate3 = LocalDate.parse(data3, formatter);

		bandeira = new Bandeira();

		bandeira.setId(1);
		bandeira.setNome("VISA");

		cliente = new Cliente();

		cliente.setId(1);
		cliente.setNome("Breno");
		cliente.setTipoPessoa('F');
		cliente.setCpfCNPJ("12345678910");

		cartao = new Cartao();

		cartao.setNumeroCartao("5412123456781234");
		cartao.setCartaoAtivo(true);
		cartao.setBandeira(bandeira);
		cartao.setCliente(cliente);
		cartao.setDataFechamento(23);

		lancamento = new Lancamento();

		lancamento.setCartao(cartao);
		lancamento.setData(localDate1);
		lancamento.setDescricao("Compra na padaria");
		lancamento.setId(1);
		lancamento.setValor(13.40);
	}

	@Test
	public void cadastrarLancamento() {

		when(lancamentoRepository.save(lancamento)).thenReturn(lancamento);
		when(cartaoRepository.findById(cartao.getNumeroCartao())).thenReturn(Optional.of(cartao));

		Lancamento lancamentoSalvo = sujeito.cadastrarLancamento(lancamento);

		assertEquals(lancamentoSalvo.getCartao(), lancamento.getCartao());
		assertEquals(lancamentoSalvo.getData(), lancamento.getData());
		assertEquals(lancamentoSalvo.getDescricao(), lancamento.getDescricao());
		assertEquals(lancamentoSalvo.getId(), lancamento.getId());
		assertEquals(lancamentoSalvo.getValor(), lancamento.getValor(), 0);
	}

	@Test
	public void deveRetornarListaDeLancamentos() {
		List<Lancamento> lancamentos = Lists.list(lancamento);

		when(lancamentoRepository.findAll()).thenReturn(lancamentos);

		List<Lancamento> lancamentosEncontrados = Lists.newArrayList(sujeito.listarLancamentos());

		assertEquals(1, lancamentosEncontrados.size());
		assertEquals(lancamento, lancamentosEncontrados.get(0));

	}

	@Test
	public void deveRetornarUmLancamentoEspecifico() {

		when(lancamentoRepository.findById(lancamento.getId())).thenReturn(Optional.of(lancamento));

		Lancamento lancamentoSalvo = sujeito.obterLancamento(lancamento.getId());
		assertEquals(lancamentoSalvo.getId(), lancamento.getId());

	}

	@Test(expected = Exception.class)
	public void naoDeveRetornarUmLancamentoEspecifico() {

		when(lancamentoRepository.findById(2l)).thenReturn(Optional.empty());

		Lancamento lancamentoSalvo = sujeito.obterLancamento(lancamento.getId());
		assertEquals(lancamentoSalvo.getId(), lancamento.getId());

	}

	@Test
	public void cadastrarLancamentoParaCartaoInvalido() {

		when(lancamentoRepository.save(lancamento)).thenReturn(lancamento);
		when(cartaoRepository.findById(cartao.getNumeroCartao())).thenReturn(Optional.empty());

		Lancamento lancamentoSalvo = sujeito.cadastrarLancamento(lancamento);
		assertNull(lancamentoSalvo);
	}
	
	@Test
	public void DeletarLancamento() {

		sujeito.deletarLancamento(lancamento.getId());
	}
}
