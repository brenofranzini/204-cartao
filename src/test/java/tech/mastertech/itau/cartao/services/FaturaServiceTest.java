package tech.mastertech.itau.cartao.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.cartao.dto.Bandeira;
import tech.mastertech.itau.cartao.dto.Cartao;
import tech.mastertech.itau.cartao.dto.Cliente;
import tech.mastertech.itau.cartao.dto.Fatura;
import tech.mastertech.itau.cartao.dto.Lancamento;
import tech.mastertech.itau.cartao.repositories.BandeiraRepository;
import tech.mastertech.itau.cartao.repositories.CartaoRepository;
import tech.mastertech.itau.cartao.repositories.ClienteRepository;
import tech.mastertech.itau.cartao.repositories.LancamentoRepository;

@ContextConfiguration(classes = FaturaService.class)
@RunWith(SpringRunner.class)
public class FaturaServiceTest {

	private Fatura fatura;
	private Bandeira bandeira;
	private Cliente cliente;
	private Cartao cartao;
	private List<Lancamento> lancamentos;

	@Autowired
	private FaturaService sujeito;

	@MockBean
	private LancamentoRepository lancamentoRepository;

	@MockBean
	private ClienteRepository clienteRepository;

	@MockBean
	private BandeiraRepository bandeiraRepository;

	@MockBean
	private CartaoRepository cartaoRepository;

	@Before
	public void preparar() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		String data1 = "2019-05-27";
		String data2 = "2019-05-23";
		String data3 = "2019-05-20";

		LocalDate localDate1 = LocalDate.parse(data1, formatter);
		LocalDate localDate2 = LocalDate.parse(data2, formatter);
		LocalDate localDate3 = LocalDate.parse(data3, formatter);

		bandeira = new Bandeira();

		bandeira.setId(1);
		bandeira.setNome("VISA");

		cliente = new Cliente();

		cliente.setId(1);
		cliente.setNome("Breno");
		cliente.setTipoPessoa('F');
		cliente.setCpfCNPJ("12345678910");

		cartao = new Cartao();

		cartao.setNumeroCartao("5412123456781234");
		cartao.setCartaoAtivo(true);
		cartao.setBandeira(bandeira);
		cartao.setCliente(cliente);
		cartao.setDataFechamento(23);

		lancamentos = new ArrayList<>();

		Lancamento lancamento1 = new Lancamento();
		lancamento1.setCartao(cartao);
		lancamento1.setData(localDate1);
		lancamento1.setDescricao("Compra na padaria");
		lancamento1.setId(1);
		lancamento1.setValor(13.40);

		Lancamento lancamento2 = new Lancamento();
		lancamento2.setCartao(cartao);
		lancamento2.setData(localDate2);
		lancamento2.setDescricao("Compra no mercado");
		lancamento2.setId(2);
		lancamento2.setValor(5.40);

		Lancamento lancamento3 = new Lancamento();
		lancamento3.setCartao(cartao);
		lancamento3.setData(localDate3);
		lancamento3.setDescricao("Compra na farmacia");
		lancamento3.setId(3);
		lancamento3.setValor(10.25);

		lancamentos.add(lancamento1);
		lancamentos.add(lancamento2);
		lancamentos.add(lancamento3);

		fatura = new Fatura();
		
		fatura.setCartao(cartao);
		fatura.setCliente(cliente);
		fatura.setDiaFechamento(cartao.getDataFechamento());
		fatura.setLancamentos(lancamentos);
	}

	@Test
	public void cadastrarFaturaDeveRetornarUmaListaComLancamentosValidos() {

		when(cartaoRepository.findById(cartao.getNumeroCartao())).thenReturn(Optional.of(cartao));
		when(lancamentoRepository.findByCartao(cartao)).thenReturn(lancamentos);

		Fatura faturaSalvo = sujeito.gerarFatura(cartao.getNumeroCartao());
		List<Lancamento> lSalva = Lists.newArrayList(faturaSalvo.getLancamentos());

		assertEquals(faturaSalvo.getCartao(), fatura.getCartao());
		assertEquals(faturaSalvo.getCliente(), fatura.getCliente());
		assertEquals(faturaSalvo.getDiaFechamento(), fatura.getDiaFechamento());
		
		assertTrue(lSalva.size() > 0);
		assertTrue(lSalva.size() == 2);

	}

	@Test
	public void cadastrarFaturaDeveRetornarUmaListaSemLancamentos() {

		lancamentos = new ArrayList<>();

		when(cartaoRepository.findById(cartao.getNumeroCartao())).thenReturn(Optional.of(cartao));
		when(lancamentoRepository.findByCartao(cartao)).thenReturn(lancamentos);

		Fatura faturaSalvo = sujeito.gerarFatura(cartao.getNumeroCartao());
		List<Lancamento> lSalva = Lists.newArrayList(faturaSalvo.getLancamentos());

		assertEquals(faturaSalvo.getCartao(), fatura.getCartao());
		assertEquals(faturaSalvo.getCliente(), fatura.getCliente());
		assertEquals(faturaSalvo.getDiaFechamento(), fatura.getDiaFechamento());
		
		assertTrue(lSalva.size() == 0);
	}
}