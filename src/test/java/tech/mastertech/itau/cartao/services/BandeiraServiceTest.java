package tech.mastertech.itau.cartao.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.cartao.dto.Bandeira;
import tech.mastertech.itau.cartao.repositories.BandeiraRepository;

@ContextConfiguration(classes = BandeiraService.class)
@RunWith(SpringRunner.class)
public class BandeiraServiceTest {
	
	private Bandeira bandeira;

	@Autowired
	private BandeiraService sujeito;

	@MockBean
	private BandeiraRepository bandeiraRepository;
	
	@Before
	public void preparar() {
		bandeira = new Bandeira();
		bandeira.setNome("MASTERCARD");
		bandeira.setId(1);
	}

	@Test
	public void cadastrarBandeira() {

		when(bandeiraRepository.save(bandeira)).thenReturn(bandeira);

		Bandeira bandeiraSalvo = sujeito.cadastrarBandeira(bandeira);
		assertEquals(bandeiraSalvo.getNome(), bandeira.getNome());
	}

	@Test
	public void deveRetornarListaDeBandeiras() {
		List<Bandeira> bandeiras = Lists.list(bandeira);

		when(bandeiraRepository.findAll()).thenReturn(bandeiras);

		List<Bandeira> bandeirasEncontrados = Lists.newArrayList(sujeito.listarBandeiras());

		assertEquals(1, bandeirasEncontrados.size());
		assertEquals(bandeira, bandeirasEncontrados.get(0));

	}

	@Test
	public void deveRetornarUmBandeiraEspecifico() {
 
//		when(bandeiraRepository.findById(bandeira.getId()).get()).thenReturn(bandeira);
		when(bandeiraRepository.findById(bandeira.getId())).thenReturn(Optional.of(bandeira));

		Bandeira bandeiraSalvo = sujeito.obterBandeira(bandeira.getId());
		assertEquals(bandeiraSalvo.getNome(), bandeira.getNome());

	}
	
	@Test
	(expected=Exception.class)
	public void naoDeveRetornarUmBandeiraEspecifico() {

//		when(bandeiraRepository.findById(bandeira.getId()).get()).thenReturn(bandeira);
		when(bandeiraRepository.findById(2)).thenReturn(Optional.empty());

		Bandeira bandeiraSalvo = sujeito.obterBandeira(bandeira.getId());
		assertEquals(bandeiraSalvo.getNome(), bandeira.getNome());

	}
}
